﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.DirectX.AudioVideoPlayback;

using AForge;
using AForge.Video;
using AForge.Video.DirectShow;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        FileVideoSource src;
        private void Form1_Load(object sender, EventArgs e)
        {
            //OpenFileDialog ofd = new OpenFileDialog();
            //ofd.ShowDialog();
            //var vid = new Video(ofd.FileName);

            //vid.Owner = pictureBox1;   


            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();

            src = new FileVideoSource(ofd.FileName);
            src.NewFrame += new NewFrameEventHandler(src_NewFrame);
            src.Start();
        }
        void src_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bitmap = eventArgs.Frame;
            pictureBox1.Image = bitmap;
            // throw new NotImplementedException();
        }
    }
}
