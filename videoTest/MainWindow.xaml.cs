﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Video = Microsoft.DirectX.AudioVideoPlayback.Video;
using System.IO;
using System.Windows.Forms;



using AForge.Video;
//using AForge.Video.DirectShow;
using AForge;
using AForge.Video.FFMPEG;

using System.Drawing;

namespace videoTest
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        System.Windows.Forms.PictureBox picturebox1;

        
        public MainWindow()
        {
            InitializeComponent();
            picturebox1 = new System.Windows.Forms.PictureBox();
            windowsFormsHost1.Child = picturebox1;
           // picturebox1.Paint += new System.Windows.Forms.PaintEventHandler(picturebox1_Paint);

            //src.NewFrame += new NewFrameEventHandler(src_NewFrame);
        }

        void picturebox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(@"C:\Temp\test.jpg");
            System.Drawing.Point ulPoint = new System.Drawing.Point(0, 0);
            e.Graphics.DrawImage(bmp, ulPoint);
        }

        
        
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
          //  var file = @"d:\Prog\WPFApp\VIDEOhelper-6.0-GOOD\FirstFloor.ModernUI.App\bin\Debug\Images\videos\yad.mp4";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();

            AForge.Video.FFMPEG.VideoFileSource src = new AForge.Video.FFMPEG.VideoFileSource(ofd.FileName);
            src.NewFrame += new NewFrameEventHandler(src_NewFrame);
            src.PlayingFinished += src_PlayingFinished;
            src.Start();
            //while (src.IsRunning)
          //  {
          //      int i = 0;
          //  }
           
          //  var vid = new Video(ofd.FileName);

           // vid.Owner = picturebox1;   
        }

        void src_PlayingFinished(object sender, ReasonToFinishPlaying reason)
        {
            AForge.Video.FFMPEG.VideoFileSource src = new AForge.Video.FFMPEG.VideoFileSource(@"d:\Prog\WPFApp\VIDEOhelper-3.0-files_LAST\FirstFloor.ModernUI.App\bin\Debug\Images\videos\gase.mp4");
            src.NewFrame += new NewFrameEventHandler(src_NewFrame);
            src.PlayingFinished += src_PlayingFinished;
            src.Start();
            ///finish
           // throw new NotImplementedException();
        }


        void src_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bitmap = eventArgs.Frame;
             ImageSourceConverter c = new ImageSourceConverter();
             this.Dispatcher.Invoke(new Action(() =>
             {
                 imgFrame.Source = BitmapToImageSource(bitmap);
             }));   
           
          //  picturebox1.Image = bitmap;
           // throw new NotImplementedException();
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }
    }
}
