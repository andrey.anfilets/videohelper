﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Xml;
using System.Xml.Serialization;

using System.IO;
using System.Net;
using System.Net.Sockets;

namespace fileSendReceive
{
    public partial class server : Form
    {
        public server()
        {
            InitializeComponent();
        }

        private void server_Load(object sender, EventArgs e)
        {
            backgroundWorker1.DoWork += BackgroundWorker1_DoWork;
            backgroundWorker1.RunWorkerCompleted += BackgroundWorker1_RunWorkerCompleted;
            backgroundWorker1.RunWorkerAsync();
        }

        private void BackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pictureBox1.ImageLocation = FileName;
            backgroundWorker1.RunWorkerAsync();
        }
        private void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            startServer();
        }

        public String FileName = "";
        public void startServer()
        {
            TcpListener list;
            Int32 port1 = 30444;
            list = new TcpListener(port1);
            list.Start();
            TcpClient client = list.AcceptTcpClient();
            Console.WriteLine("Client trying to connect");
            Stream stream = client.GetStream();
            XmlSerializer mySerializer = new XmlSerializer(typeof(FileTransfer));
            FileTransfer myObject = (FileTransfer)mySerializer.Deserialize(stream);
            Console.WriteLine("name: " + myObject.Name);
            // FileStream str = new FileStream(myObject.Name, FileMode.Create);
            File.WriteAllBytes("rec\\" + myObject.Name, System.Convert.FromBase64String(myObject.Content));

           // str.Write(Encoding.ASCII.GetBytes(myObject.Content),0,myObject.Size);
            //str.Close();
            FileName = myObject.Name;
            list.Stop();
            client.Close();
        }
    }
}
