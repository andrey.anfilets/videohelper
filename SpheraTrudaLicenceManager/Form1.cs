﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;


namespace SpheraTrudaLicenceManager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "Введите код для получения лицензии")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Black;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            string key = textBox1.Text.TrimEnd(' ');
            textBox2.Text = CalculateMD5Hash(key);
            textBox2.ForeColor = Color.Black;
            if (MessageBox.Show("Сохранить файл с кодом?", "Сохранить", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.AddExtension = true;
                sfd.DefaultExt = "txt";
                sfd.FileName = DateTime.Now.ToShortDateString() + "_VIDEOHELPER_LICENCE_BERG.txt";
                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    System.IO.File.WriteAllText(sfd.FileName,textBox2.Text);
                }
            
            }    
            }
        public string CalculateMD5Hash(string input)

{

    // step 1, calculate MD5 hash from input

    MD5 md5 = System.Security.Cryptography.MD5.Create();

    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

    byte[] hash = md5.ComputeHash(inputBytes);

    // step 2, convert byte array to hex string

    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < hash.Length; i++)

    {

        sb.Append(hash[i].ToString("X2"));

    }

    return sb.ToString();

}

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox2.Text);
            toolStripStatusLabel1.Text = "Код скопирован в буффер обмена";
        }
    }
}
